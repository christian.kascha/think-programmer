/* 
   get main behavior and control flow right.
*/ 
#include <iostream>                    
#include <string>                    
#include <vector>


int main()                             
{                                      

  char ichar; 
  int  inumber;
  char answer; 
  bool is_valid_input {true};
  
  while (true) {
    std::cout << "Please type in some numbers, separated by <<,>>." << std::endl; 


    while (true) {
      ichar = std::cin.get();
      
      if (isdigit(ichar)) {
	std::cin.putback(ichar);
	std::cin >> inumber;

      }
      else if (ichar == ',') {
	// do not do anything
      }
      else if (int(ichar) == 10) {
	break; 
      }
      else {
	is_valid_input = false;
	break; 
      }
    }

    if (is_valid_input) {
      std::cout << "result" << std::endl; 
    }
    else {
      std::cout << "input not valid" << std::endl; 
    }



    while (true) {
      std::cout << "Would you like to continue?" << std::endl;
      std::cin >> answer;

      if (not std::cin or (answer != 'y' and answer != 'n')) {
	std::cout << "Please answer y/n" << std::endl;
	continue;
      }
      else {
	ichar = std::cin.get();
	break;
      }
    }
    if (answer == 'n') {
      break;
    }
  }
  

  return 0;                           
}  
