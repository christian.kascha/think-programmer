/*
Test mode switching.
 */ 
#include <iostream>                    
#include <string>                    
#include <vector>

using namespace std;

class Decoder
{
public:
  Decoder();
  string get_mode(){
    return v_modes.at(mode);
  }
  void switch_mode(){
    int n = 3;
    mode++;
    mode = mode % n; 

  }

private:
  int mode; 
  std::vector<string> v_modes; 

};

Decoder::Decoder(){
  v_modes = {"uppercase", "lowercase", "punctuation"};
  mode = 0;
}


int main()                             
{                                      
  Decoder MyDecoder;
  cout << MyDecoder.get_mode() << endl; 
  MyDecoder.switch_mode();
  cout << MyDecoder.get_mode() << endl; 
  MyDecoder.switch_mode();
  cout << MyDecoder.get_mode() << endl; 
  MyDecoder.switch_mode();
  cout << MyDecoder.get_mode() << endl; 
  MyDecoder.switch_mode();
  cout << MyDecoder.get_mode() << endl; 
  MyDecoder.switch_mode();
  cout << MyDecoder.get_mode() << endl; 

  return 0;                           
}  
