#include "gtest/gtest.h"
#include "formula.h"

TEST(DecodingTest, uppercaseTest) {
  EXPECT_EQ (decode_to_uppercase(1), 'A');
  EXPECT_EQ (decode_to_uppercase(26), 'Z');
  EXPECT_ANY_THROW (decode_to_uppercase(0));
  EXPECT_ANY_THROW (decode_to_uppercase(27));
}

TEST(DecodingTest, lowercaseTest) {
  EXPECT_EQ (decode_to_lowercase(1), 'a');
  EXPECT_EQ (decode_to_lowercase(26), 'z');
  EXPECT_ANY_THROW (decode_to_lowercase(0));
  EXPECT_ANY_THROW (decode_to_lowercase(27));
}

TEST(DecodingTest, punctuationTest) {
  EXPECT_EQ (decode_to_punctuation(1), '!');
  EXPECT_EQ (decode_to_punctuation(2), '?');
  EXPECT_EQ (decode_to_punctuation(3), ',');
  EXPECT_EQ (decode_to_punctuation(4), '.');
  EXPECT_EQ (decode_to_punctuation(5), ' ');
  EXPECT_EQ (decode_to_punctuation(6), ';');
  EXPECT_EQ (decode_to_punctuation(7), '"');
  EXPECT_EQ (decode_to_punctuation(8), '\'');
  EXPECT_ANY_THROW (decode_to_punctuation(0));
  EXPECT_ANY_THROW (decode_to_punctuation(9));
}



