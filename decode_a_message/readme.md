Solution to the “Decoding Problem” of the book /Think like a programmer/. 


-- prepare
download latest version of googletest 

-- building
cd build/ 
cmake .. 
make all 

-- testing 
cd build/
./tst/{project_name}_test

