#include "formula.h"
#include <iostream>


Decoder::Decoder(){
  v_modes = {"uppercase", "lowercase", "punctuation"};
  mode = 0;
}

void Decoder::print(){
  std::cout << decoded_txt << std::endl; 
}

void Decoder::flush(){
  decoded_txt = "";
  mode = 0; 
}

void Decoder::process_int(int integer){

  if (isRemainderZero(integer)) {
    switch_mode();
  }
  else {
    decode_int(integer);
  }
}

bool Decoder::isRemainderZero(int integer){
  int divider;
     
  if (mode == 0 or mode == 1) {
    divider = 27; 
  }
  else {
    divider = 9; 
  }

  return ((integer % divider)==0);
    }


void Decoder::switch_mode(){
  int n = 3;
  mode++;
  mode = mode % n; 
}

void Decoder::decode_int(int integer){
  char result = ' ';

  switch (mode) {
  case 0: {
    result = to_uppercase(integer % 27);
    break;
  }
  case 1: {
    result = to_lowercase(integer % 27);
    break;
  }
  case 2: {
    result = to_punctuation(integer % 9);
    break;
  }
  default:
    break;
  }

 decoded_txt += result;
}

char Decoder::to_uppercase(int integer)
{
  class IntegerNotinRange {};

  if (integer < 1 or integer > 26) {
    throw IntegerNotinRange{};
  }
  return char(integer+64);
}

char Decoder::to_lowercase(int integer)
{
  class IntegerNotinRange {};

  if (integer < 1 or integer > 26) {
    throw IntegerNotinRange{};
  }
  return char(integer+96);
}


char Decoder::to_punctuation(int integer){
  class IntegerNotinRange {};

  switch (integer) {
  case 1: {
    return '!';
  }
  case 2: {
    return '?';
  }
  case 3: {
    return ',';
    break;
  }
  case 4: {
    return '.';
    break;
  }
  case 5: {
    return ' ';
    break;
  }
  case 6: {
    return ';'; 
    break;
  }
  case 7: {
    return '"'; 
    break;
  }
  case 8: {
    return '\'';
    break;
  }
  default:
    throw IntegerNotinRange{};
  }
}


std::string Decoder::get_mode(){
  return v_modes.at(mode);
}
