#ifndef EXAMPLEPROJECT_FORMULA_H
#define EXAMPLEPROJECT_FORMULA_H

#include<string>
#include<vector>


class Decoder
{
public:
  Decoder();
  void print();
  void flush();
  void process_int(int integer);

private:
  int mode; 
  std::vector<std::string> v_modes; 
  std::string decoded_txt; 

  std::string get_mode();

  void switch_mode();

  bool isRemainderZero(int integer);


  void decode_int(int integer);  
  char to_uppercase(int integer);

  char to_lowercase(int integer);

  char to_punctuation(int integer);
};




#endif //EXAMPLEPROJECT_FORMULA_H

